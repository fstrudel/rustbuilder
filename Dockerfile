FROM ekidd/rust-musl-builder

ADD . ./home/rust/src
RUN chown -R rust:rust .

CMD cargo build --release